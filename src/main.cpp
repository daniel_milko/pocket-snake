#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include "../lib/SSD1306/Framebuffer.h"
#include "../lib/Snake/Snake.h"

int main(void) {

    DDRB &= ~(1<<DDB2);
    PORTB |= (1<<PORTB2);
    DDRB &= ~(1<<DDB1);
    PORTB |= (1<<PORTB1);
    DDRB &= ~(1<<DDB0);
    PORTB |= (1<<PORTB0);
    DDRD &= ~(1<<DDD7);
    PORTD |= (1<<PORTD7);

    Framebuffer fb;
    Snake snake;
    fb.displayArt(2);
    fb.show();
    _delay_ms(3000);
    for(;;){
        
        fb.clear();

        DDRD |= (1<<DDD6);

        
        fb.drawText(0,0,snake.textScore);
        char charArrayToReturn[8];
        itoa(snake.scoreInt,charArrayToReturn,10);
        fb.drawText(56,0,charArrayToReturn);
        fb.drawPixel(snake.pointX,snake.pointY);
        fb.drawRectangle(0,10,127,63);
        for(uint8_t i=0; i < snake.snakeLength; i++){
            fb.drawPixel(snake.snakePosition[i][0], snake.snakePosition[i][1]);
        }
        fb.show();

        bool isPushed = false;
        uint8_t err = 50;

        if( !(PINB & (1<<PORTB2))){
            if(snake.currentDirection != 0){
                isPushed = true;
                err = snake.move(0);
            }
		}

        if( !(PINB & (1<<PORTB1))){
            if(snake.currentDirection != 1){
                isPushed = true;
                err = snake.move(1);
            }
		}

        if( !(PINB & (1<<PORTB0))){
            if(snake.currentDirection != 2){
                isPushed = true;
                err = snake.move(2);
            }
		}

        if( !(PIND & (1<<PORTD7))){
            if(snake.currentDirection != 3){
                isPushed = true;
                err = snake.move(3);
            }
		}

        

        if(!isPushed || err == 3){
            err = snake.move(snake.currentDirection);
        }
        

        if(err == 1){
            fb.clear();
            fb.displayArt(1);
            fb.show();
            PORTD |= (1<<PORTD6);
            _delay_ms(200);
            PORTD &= ~(1<<PORTD6);
            _delay_ms(200);
            PORTD |= (1<<PORTD6);
            _delay_ms(200);
            PORTD &= ~(1<<PORTD6);
            _delay_ms(200);
            PORTD |= (1<<PORTD6);
            _delay_ms(1000);
            PORTD &= ~(1<<PORTD6);
            return 0;
        }

    }

    return 0;
}