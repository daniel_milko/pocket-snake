#include "Snake.h"

uint8_t Snake::move(uint8_t direction){
    //DIRECTION
    // 0 - RIGHT
    // 1 - DOWN
    // 2 - LEFT
    // 3 - UP

    int snakeLengthBuff = this->snakeLength;
    snakeLengthBuff--;
    uint8_t posX = this->snakePosition[snakeLengthBuff][0];
    uint8_t posY = this->snakePosition[snakeLengthBuff][1];
    switch (direction)
    {
        case 0:
            if(this->currentDirection == 2) return 3;
            posX++;
            if(posX>126) return 1;
            break;
        case 1:
            if(this->currentDirection == 3) return 3;
            posY++;
            if(posY>62) return 1;
            break;
        case 2:
            if(this->currentDirection == 0 || this->currentDirection == -1) return 3;
            if(posX<=1) return 1;
            posX--; 
            break;
        case 3:
            if(this->currentDirection == 1) return 3;
            if(posY<=11) return 1;
            posY--;
            break;
        default:
            return 2;
            break;
    }
    this->currentDirection = direction;
    
    

    for(int i = snakeLengthBuff; i>=0; i--){
        if(posX == this->snakePosition[i][0] && posY == this->snakePosition[i][1]){
            return 1;
        }
    }

    for(int i = snakeLengthBuff; i>=0; i--){
        uint8_t buffPosX = this->snakePosition[i][0];
        uint8_t buffPosY = this->snakePosition[i][1];
        this->snakePosition[i][0] = posX;
        this->snakePosition[i][1] = posY;
        posX = buffPosX;
        posY = buffPosY;
    }
    
    if(this->snakePosition[snakeLengthBuff][0]==this->pointX && this->snakePosition[snakeLengthBuff][1]==this->pointY){
        PORTD |= (1<<PORTD6);
        this->detectCollision();

    }else{
        PORTD &= ~(1<<PORTD6);
    }

    return 0;
}

Snake::Snake(){
    uint8_t xPosStarter = 10;
    this->snakePosition = (uint8_t **)malloc((this->snakeLength) * sizeof(uint8_t*));
    for(uint8_t i=0; i<this->snakeLength; i++){
        this->snakePosition[i] = (uint8_t *)malloc((2) * sizeof(uint8_t));
        this->snakePosition[i][0] = xPosStarter;
        this->snakePosition[i][1] = 20;
        xPosStarter++;
    }

    
    this->generatePoint();
}

void Snake::generatePoint(){

    int snakeLengthBuff = this->snakeLength;
    snakeLengthBuff--;

    bool pointInSnake = true;
    int generatedX = rand() % 127;
    int generatedY = rand() % 51 + 11;

    while(pointInSnake){
        generatedX = rand() % 127;
        generatedY = rand() % 51 + 11;
        pointInSnake = false;
        for(int i = snakeLengthBuff; i>=0; i--){
            if(generatedX == this->snakePosition[i][0] && generatedY == this->snakePosition[i][1]){
                pointInSnake = true;
                break;
            }
        }
    }
    this->pointX = generatedX; 
    this->pointY = generatedY; 

}

void Snake::detectCollision(){

    this->snakePosition = (uint8_t**) realloc(this->snakePosition,(this->snakeLength+1 )* sizeof(uint8_t*));
    this->snakePosition[this->snakeLength] = (uint8_t *)malloc((2) * sizeof(uint8_t));
    this->snakePosition[this->snakeLength][0] = this->pointX;
    this->snakePosition[this->snakeLength][1] = this->pointY;

    this->snakeLength++;
    this->scoreInt++;
    this->generatePoint();
}