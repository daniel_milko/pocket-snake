#include <iostream>
#include <fstream>

extern "C" {
    #define STB_IMAGE_IMPLEMENTATION
    #include "stb_image.h"
}

using namespace std;

int main()
{
    std::string filename = "image2.png";
    
    // ... x = width, y = height, n = # 8-bit components per pixel ...
    // ... replace '0' with '1'..'4' to force that many components per pixel
    // ... but 'n' will always be the number that it would have been if you said 0
    int x,y,n;
    unsigned char *data = stbi_load(filename.c_str(), &x, &y, &n, 0);

    ofstream myfile;
    myfile.open ("commands2.txt");
    
    
    // ... process data if not NULL ..
    if (data != nullptr && x > 0 && y > 0)
    {
        if (n == 3)
        {
            
            std::cout << "First pixel: RGB "
                      << static_cast<int>(data[0]) << " "
                      << static_cast<int>(data[1]) << " "
                      << static_cast<int>(data[2]);
        }
        else if (n == 4)
        {
            int tabLength = (x*y)*4;
            int pixelNumber = 0;
            int pixelX;
            int pixelY;
            int buff;
            for(int i=0; i < tabLength; i+=4){
                if(static_cast<int>(data[i]) == 0) {
                    pixelX = pixelNumber / 128;
                    buff = pixelX*128;
                    pixelY = pixelNumber - buff;
                    // std::cout << "(" << pixelY << "," << pixelX << ")" << static_cast<int>(data[i]) << " " << static_cast<int>(data[i+1]) << " " << static_cast<int>(data[i+2]) << " " << static_cast<int>(data[i+3]) << '\n';
                    myfile << "this->drawPixel" << "(" << pixelY << "," << pixelX << ");" << '\n';
                    
                }
                pixelNumber++;
                
            }
            std::cout << pixelNumber;
            
        }
    }
    else
    {
        std::cout << "Some error\n";
    }
    myfile.close();
    stbi_image_free(data);
}