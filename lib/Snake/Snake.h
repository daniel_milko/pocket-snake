#include <stdint.h>
#include <avr/io.h>
#include <stdlib.h>

class Snake {
    public:
        uint8_t** snakePosition = NULL;
        int snakeLength = 10;
        int currentDirection = -1;
        int pointX = 0;
        int pointY = 0;
        int scoreInt = 0;
        char* textScore = "Score: ";
        uint8_t move(uint8_t);
        void generatePoint(void);
        void detectCollision(void);
        Snake();
};
